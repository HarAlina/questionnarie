package com.egs.controller;

import com.egs.entity.User;
import com.egs.service.inerfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
    public class LoginCont {
        @Autowired
        UserService userService;

        @RequestMapping(value = "/login", method = RequestMethod.POST)
        public ModelAndView login(ModelAndView model, @RequestParam("email") String email, @RequestParam("password") String password) {
            int index = userService.get(email);
            if (index >= 0) {
                User user = userService.get(index);
                if (user.getPassword().equals(password)) {
                    model.addObject("user", user);
                    model.setViewName("questions");
                }else model.setViewName("redirect:/login");
            } else model.setViewName("redirect:/login");
            return model;
        }
}
