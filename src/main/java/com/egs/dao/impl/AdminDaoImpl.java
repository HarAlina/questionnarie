package com.egs.dao.impl;

import com.egs.dao.inerfaces.AdminDao;
import com.egs.entity.Admin;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AdminDaoImpl implements AdminDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void add(Admin admin) {
        sessionFactory.getCurrentSession().saveOrUpdate(admin);
    }

    @Override
    public List<Admin> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from Admin").list();
    }

    @Override
    public Admin update(Admin admin) {
        sessionFactory.getCurrentSession().update(admin);
        return admin;
    }

    @Override
    public Admin get(int id) {
        return (Admin) sessionFactory.getCurrentSession().get(Admin.class, id);
    }
    @Override
    public void delete(int id) {
        Admin admin = (Admin) sessionFactory.getCurrentSession().load(Admin.class, id);
        if (admin != null) {
            this.sessionFactory.getCurrentSession().delete(admin);
        }
    }
}
