package com.egs.dao.impl;

import com.egs.dao.inerfaces.TestResultDao;
import com.egs.entity.TestResult;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TestResultDaoImpl implements TestResultDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void add(TestResult userTest) {
        sessionFactory.getCurrentSession().saveOrUpdate(userTest);
    }

    @Override
    public void delete(int id) {
        TestResult userTest = (TestResult) sessionFactory.getCurrentSession().load(TestResult.class, id);
        if (userTest != null) {
            this.sessionFactory.getCurrentSession().delete(userTest);
        }
    }

    @Override
    public List<TestResult> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from UserTest").list();
    }

    @Override
    public TestResult update(TestResult userTest) {
        sessionFactory.getCurrentSession().update(userTest);
        return userTest;
    }

    @Override
    public TestResult get(int id) {
        return (TestResult) sessionFactory.getCurrentSession().get(TestResult.class, id);
    }
}
