package com.egs.dao.inerfaces;

import com.egs.entity.Admin;

import java.util.List;

public interface AdminDao {

    void add(Admin admin);
    void delete(int id);
    Admin update(Admin admin);
    List<Admin> getAll();
    Admin get(int id);
}
