package com.egs.dao.inerfaces;

import com.egs.entity.Question;

import java.util.List;

public interface QuestionDao {

    void add(Question question);
    List<Question> getAll();
    void delete(int id);
    Question update(Question question);
    Question get(int id);
}
