package com.egs.dao.inerfaces;

import com.egs.entity.TestResult;

import java.util.List;

public interface TestResultDao {

    void add(TestResult userTest);
    TestResult update(TestResult userTest);
    void delete(int id);
    List<TestResult> getAll();
    TestResult get(int id);
}
