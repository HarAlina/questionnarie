package com.egs.dao.inerfaces;

import com.egs.entity.User;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserDao {

    void add(User user);
    User update(User user);
    void delete(int id);
    List<User> getAll();
    User get(int id);

    @Query("SELECT id FROM User WHERE email = :email")
    int get(String email);
}
