package com.egs.entity;

import com.egs.entity.enums.Role;

import javax.persistence.*;


@Entity
@Table
public class Admin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true)
    private Long id;

    private String name;
    private String surname;

    @Enumerated(EnumType.STRING)
    private Role role;

}
