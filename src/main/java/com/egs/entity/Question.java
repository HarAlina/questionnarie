package com.egs.entity;


import com.egs.entity.enums.Fields;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private Long id;

    @Enumerated(EnumType.STRING)
    private Fields fieldType;

    @OneToMany(mappedBy = "question", cascade = CascadeType.ALL)
    private List<Test> questions;

}
