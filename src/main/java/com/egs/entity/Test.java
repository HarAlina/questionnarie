package com.egs.entity;
import com.mchange.v2.c3p0.test.TestRefSerStuff;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "test")
public class Test {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique = true)
    private Long id;

    @Column(name = "questionCount",nullable = false, length = 20)
    private int questionCount;

    @JoinColumn(name = "question_id",nullable = false)
    @ManyToOne()
    private Question question;

    @JoinColumn(name = "admin_id",nullable = false)
    @ManyToOne()
    private Admin admin;

    @OneToMany(mappedBy = "test", cascade = CascadeType.ALL)
    private List<TestResult> userTests;
}