package com.egs.entity;


import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "test_result")
public class TestResult {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique = true)
    private Long id;

    @JoinColumn(name = "user_id",nullable = false)
    @ManyToOne()
    private User user;

    @JoinColumn(name = "test_id",nullable = false)
    @ManyToOne()
    private Test test;

    @Column(name = "result",nullable = false)
    private int result;

    @Temporal(TemporalType.DATE)
    @Column(name = "startDate",nullable = false)
    private Instant startDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "endDate",nullable = false)
    private Instant endDate;
}

