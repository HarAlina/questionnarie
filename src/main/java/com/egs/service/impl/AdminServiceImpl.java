package com.egs.service.impl;

import com.egs.dao.inerfaces.AdminDao;
import com.egs.entity.Admin;
import com.egs.service.inerfaces.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.ws.ServiceMode;
import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminDao adminDAO;

    @Override
    @Transactional
    public void add(Admin admin) {
        adminDAO.add(admin);
    }

    @Override
    @Transactional
    public void delete(int id) {
        adminDAO.delete(id);
    }

    @Override
    @Transactional
    public List<Admin> getAll() {
        return adminDAO.getAll();
    }

    @Override
    @Transactional
    public Admin update(Admin admin) {
        return adminDAO.update(admin);
    }

    @Override
    @Transactional
    public Admin get(int id) {
        return adminDAO.get(id);
    }

    public void setAdminDAO(AdminDao adminDAO) {
        this.adminDAO = adminDAO;
    }

}

