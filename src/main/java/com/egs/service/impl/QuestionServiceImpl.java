package com.egs.service.impl;

import com.egs.dao.inerfaces.QuestionDao;
import com.egs.entity.Question;
import com.egs.service.inerfaces.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {
    @Autowired
    private QuestionDao questionDAO;

    @Override
    @Transactional
    public void add(Question question) {
        questionDAO.add(question);
    }

    @Override
    @Transactional
    public void delete(int id) {
        questionDAO.delete(id);
    }

    @Override
    @Transactional
    public List<Question> getAll() {
        return questionDAO.getAll();
    }

    @Override
    @Transactional
    public Question update(Question question) {
        return questionDAO.update(question);
    }

    @Override
    @Transactional
    public Question get(int id) {
        return questionDAO.get(id);
    }

    public void setQuestionDAO(QuestionDao questionDAO) {
        this.questionDAO = questionDAO;
    }
}

