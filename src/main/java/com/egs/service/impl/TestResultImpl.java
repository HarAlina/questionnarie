package com.egs.service.impl;

import com.egs.dao.inerfaces.TestResultDao;
import com.egs.dao.inerfaces.UserDao;
import com.egs.entity.TestResult;
import com.egs.entity.User;
import com.egs.service.inerfaces.TestResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TestResultImpl implements TestResultService {
    @Autowired
    private TestResultDao testResultDao;

    @Override
    @Transactional
    public void add(TestResult userTest) {
        testResultDao.add(userTest);
    }

    @Override
    @Transactional
    public void delete(int id) {
        testResultDao.delete(id);
    }

    @Override
    @Transactional
    public List<TestResult> getAll() {
        return testResultDao.getAll();
    }

    @Override
    @Transactional
    public TestResult update(TestResult userTest) {
        return testResultDao.update(userTest);
    }

    @Override
    @Transactional
    public TestResult get(int id) {
        return testResultDao.get(id);
    }

    public void setUserTestDAO(TestResultDao testResultDao) {
        this.testResultDao = testResultDao;
    }
}
