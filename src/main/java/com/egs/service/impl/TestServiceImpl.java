package com.egs.service.impl;

import com.egs.dao.inerfaces.TestDao;
import com.egs.entity.Test;
import com.egs.service.inerfaces.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TestServiceImpl implements TestService {
    @Autowired
    private TestDao testDAO;

    @Override
    @Transactional
    public void add(Test test) {
        testDAO.add(test);
    }

    @Override
    @Transactional
    public void delete(int id) {
        testDAO.delete(id);
    }

    @Override
    @Transactional
    public List<Test> getAll() {
        return testDAO.getAll();
    }

    @Override
    @Transactional
    public Test update(Test admin) {
        return testDAO.update(admin);
    }

    @Override
    @Transactional
    public Test get(int id) {
        return testDAO.get(id);
    }

    public void setTestDAO(TestDao testDAO) {
        this.testDAO = testDAO;
    }
}
