package com.egs.service.impl;

import com.egs.dao.inerfaces.UserDao;
import com.egs.entity.User;
import com.egs.service.inerfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDAO;

    @Override
    @Transactional
    public void add(User user) {
        userDAO.add(user);
    }

    @Override
    @Transactional
    public void delete(int id) {
        userDAO.delete(id);
    }

    @Override
    @Transactional
    public List<User> getAll() {
        return userDAO.getAll();
    }

    @Override
    @Transactional
    public User update(User user) {
        return userDAO.update(user);
    }

    @Override
    @Transactional
    public User get(int id) {
        return userDAO.get(id);
    }

    @Override
    public int get(String email) {
        return userDAO.get(email);
    }

    public void setUserDAO(UserDao userDAO){
        this.userDAO = userDAO;
    }

}

