package com.egs.service.inerfaces;

import com.egs.entity.Admin;

import java.util.List;

public interface AdminService {

    void add(Admin admin);
    Admin update(Admin admin);
    void delete(int id);
    Admin get(int id);
    List<Admin> getAll();

}

