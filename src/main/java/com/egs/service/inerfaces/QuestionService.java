package com.egs.service.inerfaces;

import com.egs.entity.Question;

import java.util.List;

public interface QuestionService {

    void add(Question question);
    Question update(Question question);
    void delete(int id);
    List<Question> getAll();


    Question get(int id);
}
