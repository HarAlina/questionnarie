package com.egs.service.inerfaces;

import com.egs.entity.Test;

import java.util.List;

public interface TestService {

    void add(Test test);
    Test update(Test test);
    void delete(int id);
    List<Test> getAll();
    Test get(int id);
}
