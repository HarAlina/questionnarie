package com.egs.service.inerfaces;


import com.egs.entity.User;

import java.util.List;

public interface UserService {

    void add(User user);
    User update(User user);
    void delete(int id);
    List<User> getAll();
    User get(int id);
    int get(String email);
}
